# Albanian translation for kdeutils
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdeutils package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeutils\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-05 00:46+0000\n"
"PO-Revision-Date: 2009-09-27 03:19+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-21 23:00+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE Shqip, Launchpad Contributions: Vilson Gjeci"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-shqip@yahoogroups.com,vilsongjeci@gmail.com"

#: main.cpp:25
#, kde-format
msgid "Sweeper"
msgstr "Sweeper"

#: main.cpp:27
#, kde-format
msgid "Helps clean unwanted traces the user leaves on the system."
msgstr ""

#: main.cpp:29
#, fuzzy, kde-format
#| msgid "(c) 2003-2005, Ralf Hoelzer"
msgid "© 2003-2005, Ralf Hoelzer"
msgstr "(c) 2003-2005, Ralf Hoelzer"

#: main.cpp:33
#, kde-format
msgid "Ralf Hoelzer"
msgstr "Ralf Hoelzer"

#: main.cpp:33
#, kde-format
msgid "Original author"
msgstr "Autori origjinal"

#: main.cpp:34
#, kde-format
msgid "Brian S. Stephan"
msgstr "Brian S. Stephan"

#: main.cpp:34
#, kde-format
msgid "Maintainer"
msgstr "Mirëmbajtësi"

#: main.cpp:35
#, kde-format
msgid "Benjamin Meyer"
msgstr "Benjamin Meyer"

#: main.cpp:35 privacyfunctions.h:65
#, kde-format
msgid "Thumbnail Cache"
msgstr ""

#: main.cpp:43
#, kde-format
msgid "Sweeps without user interaction"
msgstr ""

#: privacyfunctions.cpp:44 privacyfunctions.cpp:53 privacyfunctions.cpp:62
#, kde-format
msgid "A thumbnail could not be removed."
msgstr ""

#: privacyfunctions.cpp:131
#, kde-format
msgid "The file exists but could not be removed."
msgstr ""

#: privacyfunctions.cpp:210
#, kde-format
msgid "A favicon could not be removed."
msgstr ""

#: privacyfunctions.h:22
#, kde-format
msgid "Cookies"
msgstr "Cookie"

#: privacyfunctions.h:22
#, kde-format
msgid "Clears all stored cookies set by websites"
msgstr ""

#: privacyfunctions.h:37
#, kde-format
msgid "Cookie Policies"
msgstr ""

#: privacyfunctions.h:37
#, kde-format
msgid "Clears the cookie policies for all visited websites"
msgstr ""

#: privacyfunctions.h:51
#, kde-format
msgid "Saved Clipboard Contents"
msgstr ""

#: privacyfunctions.h:51
#, kde-format
msgid "Clears the clipboard contents stored by Klipper"
msgstr ""

#: privacyfunctions.h:65
#, kde-format
msgid "Clears all cached thumbnails"
msgstr ""

#: privacyfunctions.h:79
#, kde-format
msgid "Run Command History"
msgstr ""

#: privacyfunctions.h:79
#, kde-format
msgid ""
"Clears the history of commands run through the Run Command tool on the "
"desktop"
msgstr ""

#: privacyfunctions.h:93
#, kde-format
msgid "Form Completion Entries"
msgstr ""

#: privacyfunctions.h:93
#, kde-format
msgid "Clears values which were entered into forms on websites"
msgstr ""

#: privacyfunctions.h:107
#, kde-format
msgid "Web History"
msgstr "Historia e Internetit"

#: privacyfunctions.h:107
#, kde-format
msgid "Clears the history of visited websites"
msgstr ""

#: privacyfunctions.h:121
#, kde-format
msgid "Web Cache"
msgstr ""

#: privacyfunctions.h:121
#, kde-format
msgid "Clears the temporary cache of websites visited"
msgstr ""

#: privacyfunctions.h:135
#, kde-format
msgid "Recent Documents"
msgstr "Dokumentat e Fundit"

#: privacyfunctions.h:135
#, kde-format
msgid ""
"Clears the list of recently used documents from the KDE applications menu"
msgstr ""

#: privacyfunctions.h:149
#, kde-format
msgid "Favorite Icons"
msgstr ""

#: privacyfunctions.h:149
#, kde-format
msgid "Clears the FavIcons cached from visited websites"
msgstr ""

#: privacyfunctions.h:163
#, kde-format
msgid "Recent Applications"
msgstr "Programet e shfrytëzuara kohët e fundit"

#: privacyfunctions.h:163
#, kde-format
msgid "Clears the list of recently used applications from KDE menu"
msgstr ""

#: sweeper.cpp:22
#, kde-format
msgctxt "General system content"
msgid "General"
msgstr "Të Përgjithshme"

#: sweeper.cpp:23
#, kde-format
msgctxt "Web browsing content"
msgid "Web Browsing"
msgstr "Shfletimi i Internetit"

#: sweeper.cpp:100
#, kde-format
msgid ""
"You are deleting data that is potentially valuable to you. Are you sure?"
msgstr ""

#: sweeper.cpp:107
#, kde-format
msgid "Starting cleanup..."
msgstr ""

#: sweeper.cpp:111
#, kde-format
msgid "Clearing %1..."
msgstr ""

#: sweeper.cpp:116
#, kde-format
msgid "Clearing of %1 failed: %2"
msgstr ""

#: sweeper.cpp:122
#, kde-format
msgid "Clean up finished."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QTreeWidget, privacyListView)
#: sweeperdialog.ui:27
#, kde-format
msgid ""
"Check all cleanup actions you would like to perform. These will be executed "
"by pressing the button below."
msgstr ""

#. i18n: ectx: property (text), widget (QTreeWidget, privacyListView)
#: sweeperdialog.ui:37
#, kde-format
msgid "Privacy Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QTreeWidget, privacyListView)
#: sweeperdialog.ui:42
#, kde-format
msgid "Description"
msgstr "Përshkrimi"

#. i18n: ectx: property (text), widget (QPushButton, selectAllButton)
#: sweeperdialog.ui:58
#, kde-format
msgid "Select &All"
msgstr "Zgjidhi të &Gjitha"

#. i18n: ectx: property (text), widget (QPushButton, selectNoneButton)
#: sweeperdialog.ui:65
#, kde-format
msgid "Select &None"
msgstr "Mos Zgjidh &Asnjë"

#. i18n: ectx: property (whatsThis), widget (QPushButton, cleanupButton)
#: sweeperdialog.ui:88
#, kde-format
msgid "Immediately performs the cleanup actions selected above"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, cleanupButton)
#: sweeperdialog.ui:91
#, kde-format
msgid "&Clean Up"
msgstr "&Pastro"
